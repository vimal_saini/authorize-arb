<?php
ini_set("display_errors", 1);
error_reporting(E_ALL);
define("APP_DIR", dirname(__FILE__)."/app");
require_once dirname(__FILE__)."/vendor/autoload.php";

date_default_timezone_set('Asia/Kolkata');
define("AUTHORIZENET_LOG_FILE", "phplog");

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing;

$_request = Request::createFromGlobals();
$_routes = require_once APP_DIR."/routes.php";

$_context = new Routing\RequestContext();
$_context->fromRequest($_request);
$_matcher = new Routing\Matcher\UrlMatcher($routes, $_context);
$generator = new Routing\Generator\UrlGenerator($_routes, $_context);
try {
    extract($_matcher->match($_request->getPathInfo()), EXTR_SKIP);
    ob_start();
    include sprintf(APP_DIR."/pages/%s.php", $_route);
    $_response = new Response(ob_get_clean());
} catch (Routing\Exception\ResourceNotFoundException $e) {
    ob_start();
    include APP_DIR."/pages/404.php";
    $_response = new Response(ob_get_clean(), 404);
} catch (Exception $e) {
    ob_start();
    include APP_DIR."/pages/500.php";
    $_response = new Response(ob_get_clean(), 500);
}
$_response->send();
