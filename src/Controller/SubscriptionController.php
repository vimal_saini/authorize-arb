<?php
namespace App\Controller;

use App\Config\ConstantsConfig;
use net\authorize\api\constants\ANetEnvironment;
use net\authorize\api\contract\v1\ARBCancelSubscriptionRequest;
use net\authorize\api\contract\v1\ARBCreateSubscriptionRequest;
use net\authorize\api\contract\v1\ARBSubscriptionType;
use net\authorize\api\contract\v1\CreditCardType;
use net\authorize\api\contract\v1\MerchantAuthenticationType;
use net\authorize\api\contract\v1\NameAndAddressType;
use net\authorize\api\contract\v1\PaymentScheduleType;
use net\authorize\api\contract\v1\PaymentScheduleType\IntervalAType;
use net\authorize\api\contract\v1\PaymentType;
use net\authorize\api\controller\ARBCreateSubscriptionController;


/**
 * Class SubscriptionController
 * @package App\Controller
 */
class SubscriptionController
{
    /**
     * @var MerchantAuthenticationType
     */
    protected $merchantAuthenticationType;

    /**
     * @var string
     */
    protected $referenceId;

    /**
     * @var string
     */
    public $errorCode;

    /**
     * @var string
     */
    public $errorMessage;

    /**
     * @return SubscriptionController
     */
    function __construct()
	{
		$this->merchantAuthenticationType = new MerchantAuthenticationType();
        $this->merchantAuthenticationType->setName(ConstantsConfig::MERCHANT_LOGIN_ID);
        $this->merchantAuthenticationType->setTransactionKey(ConstantsConfig::MERCHANT_TRANSACTION_KEY);
	}

    /**
     * @param $intervalLength
     * @param string $intervalUnit
     * @param \DateTime $startDate
     * @param $paymentTotalOccurences
     * @param $paymentTrialOccurences
     * @param $subscriptionAmount
     * @param $trialAmount
     * @param $creditCardNumber
     * @param $creditCardExpirationMonth
     * @param $creditCardExpirationYear
     * @param $billingFirstName
     * @param $billingLastName
     * @return bool
     */
    function createSubscription($intervalLength,
                                $intervalUnit = "days",
                                \DateTime $startDate,
                                $paymentTotalOccurences,
                                $paymentTrialOccurences,
                                $subscriptionAmount,
                                $trialAmount,
                                $creditCardNumber,
                                $creditCardExpirationMonth,
                                $creditCardExpirationYear,
                                $billingFirstName,
                                $billingLastName)
    {
        $this->referenceId = "ref-".time();
        $subscription = new ARBSubscriptionType();
        $subscription->setName("Sample Subscription");

        $interval = new IntervalAType();
        $interval->setLength($intervalLength);
        $interval->setUnit($intervalUnit);

        $paymentSchedule = new PaymentScheduleType();
        $paymentSchedule->setInterval($interval);
        $paymentSchedule->setStartDate($startDate);
        $paymentSchedule->setTotalOccurrences($paymentTotalOccurences);
        $paymentSchedule->setTrialOccurrences($paymentTrialOccurences);

        $subscription->setPaymentSchedule($paymentSchedule);
        $subscription->setAmount($subscriptionAmount);
        $subscription->setTrialAmount($trialAmount);

        $creditCard = new CreditCardType();
        $creditCard->setCardNumber($creditCardNumber);
        $expirationDate = "{$creditCardExpirationYear}-{$creditCardExpirationMonth}";
        $creditCard->setExpirationDate($expirationDate);

        $paymentType = new PaymentType();
        $paymentType->setCreditCard($creditCard);

        $subscription->setPayment($paymentType);

        $billTo = new NameAndAddressType();
        $billTo->setFirstName($billingFirstName);
        $billTo->setLastName($billingLastName);

        $subscription->setBillTo($billTo);

        $request = new ARBCreateSubscriptionRequest();
        $request->setMerchantAuthentication($this->merchantAuthenticationType);
        $request->setRefId($this->referenceId);
        $request->setSubscription($subscription);

        $controller = new ARBCreateSubscriptionController($request);
        $ARBEnviornment = (ConstantsConfig::MERCHANT_SANDBOX)? ANetEnvironment::SANDBOX:ANetEnvironment::PRODUCTION;
        $response = $controller->executeWithApiResponse($ARBEnviornment);

        if(($response != null) && ($response->getMessages()->getResultCode() == "Ok")){
            return $response->getSubscriptionId();
        }
        $errorMessages = $response->getMessages()->getMessage();
        $this->errorCode = $errorMessages[0]->getCode();
        $this->errorMessage = $errorMessages[0]->getText();
        return false;
    }

    /**
     * @param $subscriptionId
     * @return bool
     */
    function cancelSubscription($subscriptionId)
    {
        $this->referenceId = "ref-".time();

        $request = new ARBCancelSubscriptionRequest();
        $request->setMerchantAuthentication($this->merchantAuthenticationType);
        $request->setRefId($this->referenceId);
        $request->setSubscriptionId($subscriptionId);

        $controller = new ARBCreateSubscriptionController($request);
        $ARBEnviornment = (ConstantsConfig::MERCHANT_SANDBOX)? ANetEnvironment::SANDBOX:ANetEnvironment::PRODUCTION;
        $response = $controller->executeWithApiResponse($ARBEnviornment);

        if(($response != null) && ($response->getMessages()->getResultCode() == "Ok")){
            $successMessages = $response->getMessages()->getMessage();
            //echo "SUCCESS : " . $successMessages[0]->getCode() . "  " .$successMessages[0]->getText() . "\n";
            return true;
        }
        $errorMessages = $response->getMessages()->getMessage();
        $this->errorCode = $errorMessages[0]->getCode();
        $this->errorMessage = $errorMessages[0]->getText();
        return false;
    }
}