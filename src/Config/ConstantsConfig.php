<?php
namespace App\Config;

/**
 * Class Constants
 * @package App\Config
 */
class ConstantsConfig
{
    /**
     * @const Authorize merchant login ID
     */
    const MERCHANT_LOGIN_ID = "5KP3u95bQpv";

    /**
     * @const Authorize merchant transaction key
     */
    const MERCHANT_TRANSACTION_KEY = "346HZ32z3fP4hTG2";

    /**
     * @const Gateway mode
     */
    const MERCHANT_SANDBOX = true;

}