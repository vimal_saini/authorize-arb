<?php
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * @var $generator UrlGeneratorInterface
 * @var $_request Request
 */
$flashMessage = "";
$amount = 0;
if($level == 1){
    $amount = "5.00";
}elseif($level == 2){
    $amount = "10.00";
}elseif($level == 3){
    $amount = "15.00";
}
if($_request->isMethod("post")){
    $first_name = $_request->request->get("first_name");
    $last_name = $_request->request->get("last_name");
    $credit_card = $_request->request->get("card_number");
    $expiry_month = $_request->request->get("month");
    $expiry_year = $_request->request->get("year");
    $subscription = new \App\Controller\SubscriptionController();
    $SubscriptionId = $subscription->createSubscription(30, "days", new DateTime(), 12, 0, $amount, 0, $credit_card, $expiry_month, $expiry_year, $first_name, $last_name);
    if($SubscriptionId){
        $flashMessage = sprintf("Subscribed Successfully. Your Subscription ID :: %s", $SubscriptionId);
    }else{
        $flashMessage = sprintf("Error Occured [%d] :: %s", $subscription->errorCode, $subscription->errorMessage);
    }
}
?>
<!DOCTYPE html>
<html lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Authorize.net recurring billing</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="container-fluid">
    <h1 class="text-center">Authorize.net recurring billing(ARB)</h1>
    <?php if(!empty($flashMessage)){ ?>
        <div class="alert alert-info">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <strong>Alert!</strong> <?php echo $flashMessage; ?>
        </div>
    <?php } ?>
    <div class="panel panel-primary">
        <div class="panel-heading">
            <div class="pull-left">Level <?php echo $level; ?></div>
            <div class="pull-right"><strong>$<?php echo $amount; ?></strong></div>
            <div class="clearfix"></div>
        </div>
        <div class="panel-body">
            <form action="" method="POST" role="form">
                <fieldset>
                    <legend>Payment Details</legend>
                    <div class="form-group">
                        <label for="first_name">First Name</label>
                        <input type="text" name="first_name" class="form-control" id="first_name" placeholder="First name">
                    </div>

                    <div class="form-group">
                        <label for="last_name">Last Name</label>
                        <input type="text" name="last_name" class="form-control" id="first_name" placeholder="Last name">
                    </div>

                    <div class="form-group">
                        <label for="card_number">Card number</label>
                        <input type="text" value="4111111111111111" name="card_number" class="form-control" id="card_number" placeholder="Card Number">
                    </div>

                    <div class="form-group">
                        <label for="month">Card Expiry</label>
                        <div class="row">
                            <div class="col-xs-2">
                                <input type="text" value="09" name="month" class="form-control" id="month" placeholder="MM">
                            </div>
                            <div class="col-xs-2">
                                <input type="text" value="2020" name="year" class="form-control" id="year" placeholder="YYYY">
                            </div>
                        </div>
                    </div>

                </fieldset>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</div>

<!-- jQuery -->
<script src="//code.jquery.com/jquery.js"></script>
<!-- Bootstrap JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="Hello World"></script>
</body>
</html>