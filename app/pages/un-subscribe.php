<?php
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * @var $generator UrlGeneratorInterface
 * @var $_request Request
 */
$flashMessage = "";
if($_request->isMethod("post")){
    $SubscriptionId = $_request->request->get("subscription_id");
    $subscription = new \App\Controller\SubscriptionController();
    $result = $subscription->cancelSubscription($SubscriptionId);
    if($result){
        $flashMessage = sprintf("Un-Subscribed Successfully. Your Subscription ID :: %s", $SubscriptionId);
    }else{
        $flashMessage = sprintf("Error Occured [%d] :: %s", $subscription->errorCode, $subscription->errorMessage);
    }
}
?>
<!DOCTYPE html>
<html lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Authorize.net recurring billing</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="container-fluid">
    <h1 class="text-center">Authorize.net recurring billing(ARB)</h1>
    <?php if(!empty($flashMessage)){ ?>
        <div class="alert alert-info">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <strong>Alert!</strong> <?php echo $flashMessage; ?>
        </div>
    <?php } ?>
    <div class="panel panel-primary">
        <div class="panel-heading">
            Cancel Subscription
        </div>
        <div class="panel-body">
            <form action="" method="POST" role="form">
                <div class="form-group">
                    <label for="subscription_id">Subscription ID</label>
                    <input type="text" value="" name="subscription_id" class="form-control" id="subscription_id" placeholder="Subscription ID">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</div>

<!-- jQuery -->
<script src="//code.jquery.com/jquery.js"></script>
<!-- Bootstrap JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="Hello World"></script>
</body>
</html>