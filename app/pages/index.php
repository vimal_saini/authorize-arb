<?php
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * @var $generator UrlGeneratorInterface
 */
?>
<!DOCTYPE html>
<html lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Authorize.net recurring billing</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="container-fluid">
    <h1 class="text-center">Authorize.net recurring billing(ARB)</h1>
    <table class="table table-bordered table-hover">
        <thead>
        <tr>
            <th>Plan name</th>
            <th>Price</th>
            <th>Cycle</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>Level-1</td>
            <td>$5</td>
            <td>30 days</td>
            <td>
                <a href="<?php echo $generator->generate("subscribe", array("level"=>"1")); ?>" class="btn btn-sm btn-success">Subscribe</a>
            </td>
        </tr>
        <tr>
            <td>Level-2</td>
            <td>$10</td>
            <td>30 days</td>
            <td>
                <a href="<?php echo $generator->generate("subscribe", array("level"=>"2")); ?>" class="btn btn-sm btn-success">Subscribe</a>
            </td>
        </tr>
        <tr>
            <td>Level-3</td>
            <td>$15</td>
            <td>30 days</td>
            <td>
                <a href="<?php echo $generator->generate("subscribe", array("level"=>"3")); ?>" class="btn btn-sm btn-success">Subscribe</a>
            </td>
        </tr>
        </tbody>
    </table>
    <p class="text-center">
        <a href="<?php echo $generator->generate("un-subscribe", array()); ?>" class="btn btn-sm btn-danger">Cancel Subscription</a>
    </p>
</div>

<!-- jQuery -->
<script src="//code.jquery.com/jquery.js"></script>
<!-- Bootstrap JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="Hello World"></script>
</body>
</html>