<?php
use Symfony\Component\Routing;

$routes = new Routing\RouteCollection();
$routes->add('index', new Routing\Route('/'));
$routes->add('subscribe', new Routing\Route('/subscribe/level/{level}'));
$routes->add('un-subscribe', new Routing\Route('/cancel-subscription'));

return $routes;
